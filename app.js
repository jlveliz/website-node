require('dotenv').config();
const express = require('express')
const hbs = require('hbs');
const app = express();
const port = process.env.PORT;

//HandleBars
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');


//servir contenido estatico

/*esto es un middleware, se ejecuta antes que una funcion dentro de una ruta
aqui le indicamos que use como directorio estatico el 'public'
esto tiene prioridad sobre las rutas, en caso de no encontrar en el directorio va a las rutas */
app.use(express.static('public'))


app.use('/', (req, res) => {
    res.render('home', {
        nombre: 'Jorge Veliz',
        titulo: 'Curso Node'
    });
});

app.get('/elements', (req, res) => {
    res.render('elements', {
        nombre: 'Jorge Veliz',
        titulo: 'Curso Node'
    });
})

app.get('/generic', (req, res) => {
    res.render('generic', {
        nombre: 'Jorge Veliz',
        titulo: 'Curso Node'
    });
})



app.get('*', (req, res) => {
    res.sendFile(__dirname + '/public/404.html')
})
app.listen(port, () => {
    console.log(port)
})